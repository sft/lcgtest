#!/bin/bash

# Setup compiler (need lcgjenkins)
source /cvmfs/sft.cern.ch/lcg/contrib/gcc/6.2.0binutils/x86_64-centos7/setup.sh

# Setup cmake
ARCH=$(uname -m)
export PATH=/cvmfs/sft.cern.ch/lcg/contrib/CMake/3.6.0/Linux-${ARCH}/bin:${PATH}
