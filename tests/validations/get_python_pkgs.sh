#!/bin/bash
#grep -zoE "LCGPackage_Add\([[:space:]]+[[:alnum:]_-]*" CMakeLists.txt | grep -v "LCGPackage_Add(" | sed "s/ \+//g" > pypackages
#VIEW="/cvmfs/sft.cern.ch/lcg/views/dev3/Fri/x86_64-slc6-gcc62-opt"
#source $VIEW/setup.sh

PLATFORM=$1

if [ "`uname`" == "Darwin" ]; then
  # readlink on mac does not support -f flag
  readlink_flag=""
else
  readlink_flag="-f"
fi

IGNORE_PYTHON_MODULE="$IGNORE_PYTHON_MODULE future#winreg jpype#jpypex ROOT#JsMVA PyYAML#_yaml tensorflow_io_gcs_filesystem#tensorflow_io_gcs_filesystem"
if [[ ${LCG_VERSION} =~ swan|nxcals|py3|python3|cuda|dev[34]$ ]]; then
    IGNORE_PYTHON_MODULE="$IGNORE_PYTHON_MODULE agile#AGILe gosam#golem QMtest#qm google_api_python_client#apiclient"
fi
if [[ ${LCG_VERSION} =~ nxcals|cuda ]]; then
    IGNORE_PYTHON_MODULE="$IGNORE_PYTHON_MODULE hepdata_converter#hepdata_converter"
fi
if [[ ${TARGET:all} != "all" ]]; then
    IGNORE_PYTHON_MODULE="$IGNORE_PYTHON_MODULE ROOT#JupyROOT"
fi
if [[ ${PLATFORM} =~ mac  ]]; then
    IGNORE_PYTHON_MODULE="$IGNORE_PYTHON_MODULE cartopy#cartopy  globus_sdk#globus_sdk  google_auth_oauthlib#google_auth_oauthlib paramiko#paramiko"
fi
if [[ ${PLATFORM} =~ mac  ]]; then
    IGNORE_PYTHON_MODULE="$IGNORE_PYTHON_MODULE parsl#parsl PyJWT#jwt PyRDF#PyRDF qtpy#qtpy pythran#omp"
fi
if [[ ${PLATFORM} =~ mac[0-9]*arm  ]]; then
    IGNORE_PYTHON_MODULE="$IGNORE_PYTHON_MODULE keras#keras keras_tuner#keras_tuner kt_legacy#kerastuner"
fi
if [[ ${LCG_VERSION} =~ dev5lhcb ]]; then
    IGNORE_PYTHON_MODULE="$IGNORE_PYTHON_MODULE agile#AGILe gosam#golem QMtest#qm google_api_python_client#apiclient"
fi

for p in ${PYTHONPATH//:/ }; do
    for F in $p/*; do 
        B=`basename $F`
        if [[ "$F" =~ "_*" ]]; then
            #Ignore packages that begin with _
            continue
        elif [[ -d "$F"  ]] ; then
            #Packages within a directory
            if [[ -n $(find $F -maxdepth 1 -name __init__.py) ]]; then
                PKG_PATH=$(readlink ${readlink_flag} $F/__init__.py | egrep -o ".*/+${PLATFORM}")
                # if version.txt does not exist we have something like clang
                if [[ ! -f $PKG_PATH/version.txt ]]; then
                    continue
                fi
                PKG=$(cut -d'=' -f1 $PKG_PATH/version.txt) 
                if [[ ! " $IGNORE_PYTHON_MODULE " =~ " $PKG#$B " ]]; then
                    echo "$PKG;$B"
                fi
            fi
        elif [[ -f "$F" ]] && [[ "$F" =~ "*.py" ]]; then
            #Single file packages   
            PYNAME=$(echo $B | sed "s/\(\.*\).py$/$1/")
            PKG_PATH=$(readlink ${readlink_flag} $F | egrep -o ".*/+${PLATFORM}")
            PKG=$(cut -d'=' -f1 $PKG_PATH/version.txt)
            if [[ ! " $IGNORE_PYTHON_MODULE " =~ " $PKG#$PYNAME " ]]; then
                echo "$PKG;$PYNAME"
            fi
        fi
    done
done
