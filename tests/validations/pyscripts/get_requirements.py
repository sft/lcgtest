#!/usr/bin/env python
import os
import sys
from importlib import metadata
from packaging.markers import Marker
from pprint import pformat


# packages that should not be in requirements for python3.9, or other reasons
bannedPackages = ['enum34',  # backport of enum to python 3.4 and earlier
                  'pywin32',  # readme for python on win 32
                  'tfestimatornightly',  # nightly build of tensorflow_estimator, not part of stack
                  'googleauthhttplib2',  # deprecated package
                  'httplib2',            # deprecated package
                  'googlecloudstorage',  # package to access google cloud storage, shouldn't be needed
                  'larkparser',          # this is now called lark
                  '2to3',                # not in the stack, needed(?) by nose(-py3), should be needed
                  'sphinx',              # nothing should depend on sphinx at runtime, except sphinx extensions
                 ]

# packages that exist with alternative names, for reason
# for simplicity we just chain them
alternative_packages = {'tensorflow': 'tensorflow-cpu',
                        'tensorflow-cpu': 'tensorflow-cpu-aws',
                        }

def getRequiredPackages(packageName):
  """Return required and optional packages as a tuple of sets.

  Cleans entries in the list from version constraints.

  Drop entries that are depending on python version of operating system, platform

  We can use `pip check` to check for consistent versions on a global setup.
  """
  try:
    listORequirements = metadata.requires(packageName)
  except metadata.PackageNotFoundError as e:
    if alternativePackage := alternative_packages.get(packageName):
      print(f"WARNING: {packageName} failed, but we have {alternativePackage}")
      return getRequiredPackages(alternativePackage)
    raise
  required = set()
  optional = set()
  # print("Raw dependencies:\n", pformat(aList))
  if not listORequirements:  # if no required metadataFound list is None
    return required, optional
  for entry in listORequirements:
    # print("checking entry", entry)
    # if there is a python_version, os or platform constraint we ignore the package and not parse if we have the right
    # python version, os or platform to need that package
    if any(token in entry for token in ['os_name', 'platform_system', 'sys_platform']):
      continue
    if 'python_version' in entry:
      myMark = Marker(entry.split(";")[1])
      if not myMark.evaluate():
        continue
      print("Needing %s for package in this environment" % entry)
    # if extra is in the entry this is optional
    isRequired = 'extra' not in entry
    entry = entry.split('extra')[0]
    # we strip away anything that is for version constraints
    for token in '[(><=!~;':
      entry = entry.split(token, 1)[0]
    # remove hyphens, underscore
    for token in '-_':
      entry = entry.replace(token, "")

    entry = entry.strip().lower()
    if entry in bannedPackages:
      continue
    if isRequired:
      # print(entry, "is required")
      required.add(entry)
    optional.add(entry)

  return required, optional

if __name__ == "__main__":
    packageName = sys.argv[1]
    targetFile = sys.argv[2]
    extraFile = sys.argv[3]

    ir, reqset = getRequiredPackages(packageName)

    with open(targetFile, 'w') as output:
        for package in sorted(ir):
            output.write(package + "\n")
    with open(extraFile, 'w') as output:
        for package in sorted(reqset):
            output.write(package + "\n")
