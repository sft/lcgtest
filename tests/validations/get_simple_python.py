#!/bin/env python

# script to get the individual python packages from the pymodules.gen file, just a list of all packages except some that
# are not really Python packages, like ROOT, Gaudi, etc.
# other packages do no have an egg file so no metadata can be found (pywt, tensorflow_probability)

ignoredPackages = [
  'DD4hep',
  'Gaudi',
  'Geant4',
  'ROOT',
  'LCIO',
  'libsvm',
  'pywt',
  'tensorflow_probability',
  'hepmc3',
  'professor',
  'omniorb',
  'cupy',
  'nxcals_extraction_api_legacy',
  'nxcals_extraction_api',
  'cmmnbuild_pytimber',
  'lhapdf', # no longer contains metadata in python
  'yoda',  # no longer contains metadata in python
  'podio',
  'EDM4hep', # does not have metadata
  'Garfield++', # does not have metadata
  'clang',  # does not have metadata
  'sherpa', # does not have metadata
  'geneva', # does not have metadata
]

from pprint import pprint
import os
import sys

packageFile = sys.argv[1]
outputFile = sys.argv[2]
packageDict = sys.argv[3]

# read pythonPackage <-> lcgpackage dict
transDict = {}
with open(packageDict) as pacDict:
  for line in pacDict.readlines():
    if line.startswith('#'):
      continue
    pyName, lcgName = line.strip().split(':')
    transDict[lcgName] = pyName

# dictionary of (python) packageName to package as named in the path
packages = dict()
with open(packageFile) as listOfPackages:
  for line in listOfPackages.readlines():
    line = line.strip()
    packageName, packagePath = line.split(";")
    if packageName in ignoredPackages:
      continue
    if any(os.path.exists(os.path.join(aPath, packagePath)) for aPath in sys.path):
      packageName = transDict.get(packageName.replace('_', ''), packageName)
      packages[packageName] = packagePath

with open(outputFile, 'wt') as outFile:
  for packageName, packagePath in sorted(packages.items()):
    outFile.write("%s;%s\n" % (packageName, packagePath))
