#!/bin/bash

# run "pip check" for dependency version validation
# ignore some packages where we have irreconcilable version requirements

pip check \
          | grep -v "google-api-python-client 1.12.11 requires google-auth-httplib2, which is not installed." \
          | grep -v "google-api-python-client 1.12.11 requires httplib2, which is not installed." \
          | grep -v "grpcio-status 1.56.2 has requirement protobuf>=4.21.6, but you have protobuf 3.20.3." \
          | grep -v "hls4ml 0.8.1 requires tensorflow, which is not installed." \
          | grep -v "httpstan 4.4.2 has requirement lz4<4.0,>=3.1, but you have lz4 4.0.0." \
          | grep -v "itkwidgets 0.32.6 has requirement ipywidgets<8.0.0, but you have ipywidgets 8.1.2." \
          | grep -v "itkwidgets 0.32.6 has requirement traitlets<5.7.0, but you have traitlets 5.9.0." \
          | grep -v "kfp 2.8.0 requires google-cloud-storage, which is not installed." \
          | grep -v "nose-py3 1.6.3 requires 2to3, which is not installed." \
          | grep -v "nxcals [.0-9]* requires nxcals-spark-session-builder, which is not installed." \
          | grep -v "nxcals-type-stubs [.0-9]* requires nxcals-spark-session-builder, which is not installed." \
          | grep -v "pyparser 1.0 has requirement parse==1.6.5, but you have parse 1.19.0." \
          | grep -v "pyrdf 0.2.1 requires enum34, which is not installed." \
          | grep -v "pyrdf 0.2.1 requires nose, which is not installed." \
          | grep -v "pytimber [.0-9]* has requirement cmw-tracing==0.6.0, but you have cmw-tracing 0.6.1. " \
          | grep -v "pytimber [.0-9]* has requirement typing-extensions==4.5.0, but you have typing-extensions 4.9.0." \
          | grep -v "qastle 0.16.0 requires lark-parser, which is not installed." \
          | grep -v "servicex 2.8.0 has requirement idna==2.10, but you have idna 3.2." \
          | grep -v "tf-keras [.0-9]* requires tensorflow, which is not installed." \
          | grep -v "tf2onnx 1.16.1 has requirement protobuf~=3.20, but you have protobuf 4.25.4." \
          | grep -v "xgboost 2.1.3 requires nvidia-nccl-cu12, which is not installed." \
          | grep -v "zfit .* requires tensorflow, which is not installed." \
          | grep -v "DEPRECATION: .* has a non-standard version number. pip 24.0 will enforce this behaviour change" \
    || echo "No version issues found"
