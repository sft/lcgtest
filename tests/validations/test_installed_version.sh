#!/bin/env bash
# Check if the version matches the path
# Check if the root dir of package is not empty
# Check if version matches within the version.txt
PROBLEMS=0

PACKAGE=$1;shift
PATH_TO_RELEASE=$1;shift
PLATFORM=$1;shift

if [ ! -d $PATH_TO_RELEASE ]; then
    echo "Given directory ('$PATH_TO_RELEASE') does not exist or is not a directory"
    exit -1
fi

if egrep "^$PACKAGE" $PATH_TO_RELEASE/LCG_externals_$PLATFORM.txt; then
    LINE=$(egrep "^$PACKAGE" $PATH_TO_RELEASE/LCG_externals_$PLATFORM.txt)
elif egrep "^$PACKAGE" $PATH_TO_RELEASE/LCG_generators_$PLATFORM.txt; then
    LINE=$(egrep "^$PACKAGE" $PATH_TO_RELEASE/LCG_generators_$PLATFORM.txt)
fi
if [ -n "$LINE" ]; then
    PACKAGE=`echo $LINE | cut -d';' -f1`
    HASH=`echo $LINE | cut -d';' -f2`
    VERSION=`echo $LINE | cut -d';' -f3 | cut -c2-` 
    DIR=`echo $LINE | cut -d';' -f4 | cut -c2-`
    DEPENDENCIES=`echo $LINE | cut -d';' -f5`

    if [[ $DIR != *"$VERSION"* ]]; then
        echo "Package version differs from the version in path: $DIR, $PACKAGE, $VERSION"
        PROBLEMS=$((PROBLEMS + 1))
    fi

    if ! grep "$VERSION" "$DIR/version.txt" &> /dev/null  ; then
        echo "Package version differs from the version saved in file: $DIR/version.txt, $PACKAGE, $VERSION"
        PROBLEMS=$((PROBLEMS + 1))
    fi

    if [ -d "$DIR" ]; then
        if [ ! "$(ls -A $DIR)" ]; then
            PROBLEMS=$((PROBLEMS + 1))
            echo "Directory $DIR is empty"
        fi
    else
        echo "$DIR not a directory"
        PROBLEMS=$((PROBLEMS + 1))
    fi
fi

exit $PROBLEMS


