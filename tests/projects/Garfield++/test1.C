#include <iostream>
#include <cstdio>
#include <fstream>
#include <cmath>

#include <TCanvas.h>
#include <TROOT.h>
#include <TApplication.h>
#include <TH1F.h>
#include <TFile.h>

#include "Garfield/MediumMagboltz.hh"
#include "Garfield/MediumSilicon.hh"
#include "Garfield/SolidBox.hh"
#include "Garfield/GeometrySimple.hh"
#include "Garfield/ComponentConstant.hh"
#include "Garfield/ComponentUser.hh"
#include "Garfield/ComponentAnalyticField.hh"
#include "Garfield/Sensor.hh"
#include "Garfield/AvalancheMicroscopic.hh"
#include "Garfield/AvalancheMC.hh"
#include "Garfield/TrackHeed.hh"
#include "Garfield/RandomEngineRoot.hh"

using namespace Garfield;

void avalanche() {

  RandomEngineRoot randomEngine;
  randomEngine.SetSeed(12345);

  const double gap = 0.04;
  const double field = 20000.;

  MediumMagboltz gas;
  gas.SetTemperature(293.15);
  gas.SetPressure(0.5 * 760.);
  gas.SetComposition("ar", 90., "co2", 10.);
  if (!gas.Initialise()) return;
  gas.EnablePenningTransfer(0.15, 0);

  SolidBox box(0, 0, gap, 2, 2, gap);
  GeometrySimple geo;
  geo.AddSolid(&box, &gas);
  
  ComponentConstant cmp;
  cmp.SetGeometry(&geo);
  cmp.SetElectricField(0, 0, field);

  Sensor sensor;
  sensor.AddComponent(&cmp);
  
  AvalancheMicroscopic aval;
  aval.SetSensor(&sensor);
    
  aval.AvalancheElectron(0, 0, gap, 0, 1, 0, 0, 0);
  int ne = 0, ni = 0;
  aval.GetAvalancheSize(ne, ni);
  std::cout << ne << " electrons, " << ni << " ions.\n";
  unsigned int nEl = 0, nIon = 0, nAtt = 0, nInel = 0, nExc = 0, nSup = 0;
  gas.GetNumberOfElectronCollisions(nEl, nIon, nAtt, nInel, nExc, nSup);
  std::cout << "Number of collisions:\n";
  std::printf("  Elastic:       %10u\n", nEl);
  std::printf("  Ionising:      %10u\n", nIon);
  std::printf("  Attachment:    %10u\n", nAtt);
  std::printf("  Inelastic:     %10u\n", nInel);
  std::printf("  Excitation:    %10u\n", nExc);
  std::printf("  Super-elastic: %10u\n", nSup);
}

void heed() {

  RandomEngineRoot randomEngine;
  randomEngine.SetSeed(12345);

  const double gap = 1; 

  MediumMagboltz gas;
  gas.SetComposition("ar");

  SolidBox box(0, 0, gap, 2, 2, gap);
  GeometrySimple geo;
  geo.AddSolid(&box, &gas);
  
  ComponentConstant cmp;
  cmp.SetGeometry(&geo);
  cmp.SetElectricField(0, 0, 100);

  Sensor sensor;
  sensor.AddComponent(&cmp);
  
  TrackHeed heed;
  heed.SetSensor(&sensor);
  heed.SetParticle("muon");
  heed.SetMomentum(10.e9);
 
  TH1F hNe("hNe", "", 200, -0.5, 199.5); 
  for (unsigned int i = 0; i < 100; ++i) {
    heed.NewTrack(0, 0, 0, 0, 0, 0, 1);
    int nesum = 0;
    double xc = 0., yc = 0., zc = 0., tc = 0., ec = 0., foo = 0.;
    int nc = 0;
    while (heed.GetCluster(xc, yc, zc, tc, nc, ec, foo)) {
      nesum += nc;
    }
    hNe.Fill(nesum); 
  }
  std::cout << "Cluster density: " << heed.GetClusterDensity() << " / cm\n";  
  std::cout << "Stopping power: " << heed.GetStoppingPower() << " eV / cm\n";
  std::cout << "Number of electrons: " << hNe.GetMean() << " +/- " 
            << hNe.GetRMS() << "\n";
}

void analyticfield() {

  MediumMagboltz gas;
  SolidBox box(0, 0, 0, 100, 100, 100);
  GeometrySimple geo;
  geo.AddSolid(&box, &gas);
  ComponentAnalyticField cmp;
  cmp.AddWire(0,  0, 0.01,  1000, "s");
  cmp.AddWire(0,  1, 0.01, -2000, "p");
  cmp.AddWire(0, -1, 0.01, -2000, "q");

  const double x0 = 0;
  const double y0 = 0.5;
  const double z0 = 0;
  std::cout << "Probing the electric field and potential at (" 
            << x0 << ", " << y0 << ", " << z0 << ").\n";
  std::cout << "Cell type: " << cmp.GetCellType() << "\n"; 
  Medium* m;
  int status = 0; 
  double ex = 0, ey = 0, ez = 0, v = 0;
  cmp.ElectricField(0, 0.5, 0, ex, ey, ez, v, m, status);
  std::cout << "  E = (" << ex << ", " << ey << ", " << ez << ")\n"
            << "  V = " << v << "\n";

  cmp.SetPeriodicityX(2);
  std::cout << "Cell type: " << cmp.GetCellType() << "\n"; 
  cmp.ElectricField(0, 0.5, 0, ex, ey, ez, v, m, status);
  std::cout << "  E = (" << ex << ", " << ey << ", " << ez << ")\n"
            << "  V = " << v << "\n";

  cmp.AddPlaneY(-2, 0, "b");
  cmp.AddPlaneY( 2, 0, "t");
  std::cout << "Cell type: " << cmp.GetCellType() << "\n"; 
  cmp.ElectricField(0, 0.5, 0, ex, ey, ez, v, m, status);
  std::cout << "  E = (" << ex << ", " << ey << ", " << ez << ")\n"
            << "  V = " << v << "\n";
}

void eLinear(const double /*x*/, const double y, const double /*z*/,
             double& ex, double& ey, double& ez) {

  constexpr double vdep = 50.;
  constexpr double vbias = 300.;
  constexpr double gap = 200.e-4;
  ex = ez = 0.;
  ey = (vbias - vdep) / gap + 2 * y * vdep / (gap * gap);
}

void sistrip() {

  RandomEngineRoot randomEngine;
  randomEngine.SetSeed(12345);
  MediumSilicon si;

  double vx = 0, vy = 0, vz = 0;
  si.ElectronVelocity(15000, 0, 0, 0, 0, 0, vx, vy, vz);
  std::cout << "Electron drift velocity at 15 kV/cm: " << vx << "\n";
  si.HoleVelocity(15000, 0, 0, 0, 0, 0, vx, vy, vz);
  std::cout << "Hole drift velocity at 15 kV/cm: " << vx << "\n";

  constexpr double gap = 200.e-4;
  constexpr double pitch = 300.e-4; 
  SolidBox box(0, 0.5 * gap, 0, 3 * gap, 0.5 * gap, 3 * gap);
  GeometrySimple geo;
  geo.AddSolid(&box, &si);

  ComponentUser cmp;
  cmp.SetGeometry(&geo);
  cmp.SetElectricField(eLinear);

  ComponentAnalyticField wfield;
  wfield.SetGeometry(&geo);
  wfield.AddPlaneY(  0, 300, "n");
  wfield.AddPlaneY(gap,   0, "p");
  wfield.AddStripOnPlaneY('z', gap, -0.5 * pitch, 0.5 * pitch, "strip");
  wfield.AddReadout("strip");

  Sensor sensor;
  sensor.AddComponent(&cmp);
  sensor.AddElectrode(&wfield, "strip");
  sensor.SetTimeWindow(0, 0.01, 3000); 

  AvalancheMC drift;
  drift.SetSensor(&sensor);
  drift.SetDistanceSteps(1.e-4);
  drift.EnableSignalCalculation();

  TrackHeed track;
  track.SetParticle("pi");
  track.SetMomentum(10.e9);
  track.SetSensor(&sensor);
   
  track.NewTrack(0, 0, 0, 0, 0, 1, 0);
  double xc = 0., yc = 0., zc = 0., tc = 0., ec = 0., foo = 0.;
  int ne = 0, nh = 0;
  while (track.GetCluster(xc, yc, zc, tc, ne, nh, ec, foo)) {
    double x0 = 0., y0 = 0., z0 = 0., t0 = 0., e0 = 0.;
    double dx = 0., dy = 0., dz = 0.;
    for (int i = 0; i < ne; ++i) {
      track.GetElectron(i, x0, y0, z0, t0, e0, dx, dy, dz);
      drift.DriftElectron(x0, y0, z0, t0);
    }
    for (int i = 0; i < nh; ++i) {
      track.GetIon(i, x0, y0, z0, t0);
      drift.DriftHole(x0, y0, z0, t0);
    }
  }
  std::cout << "Induced current at 1 ns: " 
            << sensor.GetSignal("strip", 100) << " fC/ns\n";
  std::cout << "Induced current at 2 ns: " 
            << sensor.GetSignal("strip", 200) << " fC/ns\n";
  std::cout << "Induced current at 3 ns: " 
            << sensor.GetSignal("strip", 300) << " fC/ns\n";
}
 
int main(int argc, char * argv[]) {

  std::cout << std::string(70, '=') << "\n";
  std::cout << " Microscopic simulation of a single-electron avalanche\n";
  std::cout << std::string(70, '=') << "\n";
  avalanche();

  std::cout << std::string(70, '=') << "\n";
  std::cout << " Simulation of the primary ionization spectrum in a gas\n";
  std::cout << std::string(70, '=') << "\n";
  heed();
 
  std::cout << std::string(70, '=') << "\n";
  std::cout << " Electric field in a wire chamber\n";
  std::cout << std::string(70, '=') << "\n";
  analyticfield();

  std::cout << std::string(70, '=') << "\n";
  std::cout << " Signal in a silicon strip sensor\n";
  std::cout << std::string(70, '=') << "\n";
  sistrip();

  return 0;
}
