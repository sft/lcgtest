#!/bin/bash

# some packages like MCGenerators are one level lower
if (ls -d $1/*/*/$2 || ls -d $1/*/*/*/$2)  > /dev/null 2> /dev/null; then
    echo "Found directories, continuing"
else
    echo "ERROR: No directories with $1/*/*/$2 or $1/*/*/*/$2"
    exit 1
fi

find -L $1/*/*/$2 $1/*/*/*/$2 -name "*.la" -exec 'grep -nH "/build" {} | grep -v "build/externals/gdb"'  \; 2> /dev/null > brokenlibtools
SIZE=$(cat brokenlibtools | wc -l)
if [ $SIZE  -gt 0 ]; then
  echo "Wrong libtools:"
  cat brokenlibtools
  exit 1
fi
echo "All libtool libraries are OK."
exit 0
