#!/bin/env python

# This test does not contain deprecated packages
import __future__
import __main__
import abc
import aifc
import argparse
import array
import ast
import asynchat
import asyncore
import atexit
import audioop
import base64
import bdb
import binascii
import bisect
import bz2
import cgi
import cgitb
import chunk
import cmath
import cmd
import code
import codecs
import codeop
import collections
import colorsys
import compileall
import contextlib
import copy
import csv
import ctypes
import datetime
import decimal
import difflib
import doctest
import errno
import filecmp
import fileinput
import fnmatch
import fractions
import ftplib
import functools
import gc
import getopt
import getpass
import gettext
import glob
import gzip
import hashlib
import heapq
import hmac
import imaplib
import imghdr
import importlib
import inspect
import io
import itertools
import json
import keyword
import lib2to3
import linecache
import locale
import mailbox
import mailcap
import marshal
import math
import mimetypes
import mmap
import modulefinder
import netrc
import nntplib
import numbers
import operator
import os
import pdb
import pickle
import pickletools
import pkgutil
import platform
import plistlib
import poplib
import pprint
import profile
import pstats
import py_compile
import pyclbr
import pydoc
import quopri
import random
import re
import rlcompleter
import runpy
import sched
import select
import shelve
import shlex
import shutil
import signal
import site
import smtpd
import smtplib
import sndhdr
import socket
import sqlite3
import ssl
import stat
import string
import stringprep
import struct
import subprocess
import sunau
import symtable
import sys
import sysconfig
import tabnanny
import tarfile
import telnetlib
import tempfile
import threading
import time
import timeit
import token
import tokenize
import trace
import traceback
import turtle
import types
import unicodedata
import unittest
import uu
import uuid
import warnings
import wave
import weakref
import webbrowser
import xdrlib
import zipfile
import zipimport
import zlib

# Version dependent
if sys.version_info.major > 2 or \
   sys.version_info.major == 2 and sys.version_info.minor == 7 and sys.version_info.micro >= 9:
    import ensurepip

if sys.version_info < (3, 9):
    import dummy_threading
    import binhex
    import parser
    import symbol

if sys.version_info.major > 2:
    import builtins
    import _thread
    import copyreg
    import html.parser
    import http.client
    import http.cookiejar
    import http.cookies
    import http.server
    import io
    import pickle
    import queue
    import socketserver
    import tkinter
    import tkinter.tix
    import tkinter.ttk
    import urllib.error
    import urllib.parse
    import urllib.request
    import urllib.robotparser
    import xmlrpc.server
    print("Python 3.x")
    if sys.version_info < (3, 8): 
        import macpath
    if sys.version_info < (3, 4):
        import formatter
        import imp
    if sys.version_info < (3, 9):
        import _dummy_thread

else:
    import __builtin__
    import anydbm
    import BaseHTTPServer
    import CGIHTTPServer
    import Cookie
    import cookielib
    import copy_reg
    import cPickle
    import cStringIO
    import dbhash
    import DocXMLRPCServer
    import dumbdbm
    import dummy_thread
    import exceptions
    import HTMLParser
    import httplib
    import Queue
    import robotparser
    import SimpleHTTPServer
    import SimpleXMLRPCServer
    import SocketServer
    import StringIO
    import thread
    import Tix
    import Tkinter
    import ttk
    import urllib
    import urllib2
    import urlparse
    import UserDict
    import UserList
    import UserString
    import whichdb
    print("Python 2.x")

# OS dependent
if sys.platform.startswith('linux') or "bsd" in sys.platform or 'unix' in sys.platform or sys.platform.startswith('darwin'): # Unix
    import crypt
    import fcntl
    import grp
    # import nis -- deprecated
    import pipes
    import posix
    import pwd
    import readline
    import resource
    if sys.platform != 'darwin' :
        import spwd
    import syslog
    import termios
    import tty
    
    # OS and Version dependent
    if sys.version_info.major > 2:
        import dbm.dumb
        if sys.platform != 'darwin' :
            import dbm.gnu
        if not (os.path.exists('/etc/os-release') and re.search(r'(?<=\nID=).*', open('/etc/os-release').read()).group(0) == 'ubuntu'):
            import dbm.ndbm
    else:
        import dbm
        import gdbm

if sys.platform.startswith('linux'): # Linux
    import pty
    
elif sys.platform.startswith('win') or sys.platform.startswith('cygwin'): # Windows
    import msilib
    import msvcrt
    import winsound
    
    # OS and Version dependent
    if sys.version_info.major > 2:
        import winreg
    else:
        import _winreg
    
elif sys.platform.startswith('darwin') and sys.version_info.major <=  2: # Mac
    import findertools
    import gensuitemodule
    import MiniAEFrame
