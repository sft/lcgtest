if [[ ! "$PLATFORM" ]]; then
    export PLATFORM=`$(dirname $0)/../lcgjenkins/getPlatform.py`
fi

if [[ ! "$VIEW" ]]; then
if [[ "$LCG_VERSION" == *"dev"* ]]; then
    VIEW="/cvmfs/sft.cern.ch/lcg/views/$LCG_VERSION/latest/$PLATFORM"
else
    VIEW="/cvmfs/sft.cern.ch/lcg/views/$LCG_VERSION/$PLATFORM"
fi  
fi

if [[ $PLATFORM == *slc6* ]]; then
    export KERAS_BACKEND=theano
fi

source $VIEW/setup.sh
if [[ ! -e "test_build" ]]; then
    mkdir test_build
fi
cd test_build
ctest -DPLATFORM=$PLATFORM -DCTEST_LABELS="$PATTERN" -DBUILD_PATH=$VIEW -S ../lcgtest/lcgtest.cmake

