# Run Tests locally

Source the relevant view
```bash
source /cvmfs/sft-nightlies.cern.ch/lcg/views/dev4/latest/x86_64-el9-gcc13-opt/setup.sh
```
And run
```bash
mkdir build
cd build
cmake -D BUILD_PATH=/cvmfs/sft-nightlies.cern.ch/lcg/nightlies/dev4/$(date '+%a')/ -D PLATFORM=x86_64-el9-gcc13-opt ..
ctest
```

Use `-R pattern` to select which test(s) to run

# Ignoring Tests for packages

## Dependencies Comparison Test

* Add a package to the list in the file `tests/validations/get_simple_python.py`

### Ignore specific packages in dependencies:

* add to bannedPackages in `tests/validations/pyscripts/get_requirements.py`

## Package Module import test

* Add to one of the lists in the file `tests/validations/get_python_pkgs.sh`

## version_if_not_empty

* remove from the file `tests/template_data/lcg_packages`

## pip check

* add ignores in `tests/validations/pip_check.sh`

## Pypi vs LCGStack name

Add translation to `tests/validations/package_name.dict`
